//
//  CWHomeViewController.m
//  CarWash
//
//  Created by Carlos molina on 13/04/16.
//  Copyright © 2016 Carlos Molina. All rights reserved.
//

#import "CWHomeViewController.h"




@import GoogleMaps;

NSInteger kCWTagViewService = 40;
NSInteger kCWTagViewExtraService = 41;


@interface CWHomeViewController ()<CLLocationManagerDelegate, GMSMapViewDelegate>
@property (strong, nonatomic) CLLocationManager * locationManager;
@property (weak, nonatomic) IBOutlet GMSMapView *viewMap;
@property (strong, nonatomic)CLLocation * location;


@end

@implementation CWHomeViewController

#pragma -mark Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:23.3400554
                                                            longitude:-102.6780955
                                                                 zoom:6];
    self.viewMap.camera = camera;
    // Creates a marker in the center of the map
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 50;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

    // Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma -mark Location delegate


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (locations.count)
    {
        self.viewMap.myLocationEnabled = YES;
        self.locationManager.delegate = nil;
        self.locationManager = nil;
        CLLocation * location = [locations objectAtIndex:0];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                longitude:location.coordinate.longitude
                                                                     zoom:16];
        [self.viewMap animateToCameraPosition:camera];
    }
}


@end
