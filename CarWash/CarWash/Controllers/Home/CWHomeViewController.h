//
//  CWHomeViewController.h
//  CarWash
//
//  Created by Carlos molina on 13/04/16.
//  Copyright © 2016 Carlos Molina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface CWHomeViewController : BaseViewController

- (void)didPressOrder;

@end
