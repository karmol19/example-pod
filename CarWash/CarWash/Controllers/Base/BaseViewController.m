//
//  BaseViewController.m
//  TestTPS
//
//  Created by Artemio Olvera Medina on 16/03/16.
//  Copyright © 2016 Artemio Olvera Medina. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    //[self.navigationController setTitle:[self getTittle]];
    [self setStyle];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSString*)getTittle{
    return @"not defined method";
}

-(void)setStyle{
    NSLog(@"setStyle method is defined in subclass");
}





-(void)hidesBackButton{
    [self.navigationItem setHidesBackButton:YES];;
}

-(void)showsBackButton{
    [self.navigationItem setHidesBackButton:NO];;
}

-(void)hidesRightBarButton{
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor clearColor]];
}

-(void)showsRightBarButton{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
}

-(void)hideAllBarButton{
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor clearColor]];
}

-(void)showAllBarButton{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
    [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStyleDone];
}

-(void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)hideBarButton:(UIBarButtonItem *)button{
    [button setEnabled:NO];
    [button setTintColor:[UIColor clearColor]];
}

-(void)showBarButton:(UIBarButtonItem *)button{
    [button setEnabled:YES];
    [button setTintColor:[UIColor whiteColor]];
    [button setStyle:UIBarButtonItemStyleDone];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
@end
