//
//  BaseViewController.h
//  TestTPS
//
//  Created by Artemio Olvera Medina on 16/03/16.
//  Copyright © 2016 Artemio Olvera Medina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (NSString*)getTittle;
- (void)setStyle;
- (void)hidesBackButton;
- (void)showsBackButton;
- (void)dismiss;
- (void)hidesRightBarButton;
- (void)showsRightBarButton;
- (void)hideBarButton:(UIBarButtonItem *)button;
- (void)showBarButton:(UIBarButtonItem *)button;
- (void)hideAllBarButton;
- (void)showAllBarButton;

@end
