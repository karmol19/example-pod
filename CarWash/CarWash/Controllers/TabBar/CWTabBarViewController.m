
//
//  CWTabBarViewController.m
//  CarWash
//
//  Created by GONET on 02/03/17.
//  Copyright © 2017 Carlos Molina. All rights reserved.
//

#import "CWTabBarViewController.h"
#import "SPReachability.h"


@import Firebase;
@interface CWTabBarViewController ()<UITabBarControllerDelegate>
@end

@implementation CWTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _currentReachability = [SPReachability reachabilityForInternetConnection];
    [_currentReachability startNotifier];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}







@end
