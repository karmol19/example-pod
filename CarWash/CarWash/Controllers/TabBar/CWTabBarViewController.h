//
//  CWTabBarViewController.h
//  CarWash
//
//  Created by GONET on 02/03/17.
//  Copyright © 2017 Carlos Molina. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SPReachability;


@interface CWTabBarViewController : UITabBarController
@property (nonatomic, strong) SPReachability *currentReachability;






@end
