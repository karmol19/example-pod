//
//  Copyright iOSDeveloperTips.com All rights reserved.
//

#import "NSString+MD5.h"
#import <Foundation/Foundation.h>
@interface NSString(MD5)
 
- (NSString *)MD5;
- (NSString *)sha1;

@end